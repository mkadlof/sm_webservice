#!/usr/bin/env bash

function reset_db {
    echo "Removing migrations directories..."
    echo "================================="
    # find . -depth 2 -name 'migrations' | xargs rm -rfv                   # Mac version
    find . -mindepth 2 -maxdepth 2 -name 'migrations' | xargs rm -rfv  # linux version TODO make it work smooth


    echo -e "\nRemoving database file..."
    echo "========================="
    rm -fv db.sqlite3

    echo -e "\nRemoving tasks directories..."
    echo "=================================="
    rm -rfv tasks_data/*


    echo -e "\nMaking migrations..."
    echo "===================="

    excludes="log\|static\|templates\|venv\|sm_webservice\|tasks_data\|sm_utils\|sm_engine"
    apps=`for i in $(ls -d */); do echo ${i%%/}; done | grep -ve ${excludes}`
    for i in ${apps};
    do
        echo ./manage.py makemigrations ${i}
        ./manage.py makemigrations ${i}
    done
    ./manage.py migrate

#    echo -e "\nPopulate database"
#    echo "====================="
#    python manage.py shell < scripts_for_db/topic_redactor_questions.py
#
#    echo -e "\nGenerate some artificial data"
#    echo "====================="
#    python manage.py shell < scripts_for_dev_db/cfa.py
#    python manage.py shell < scripts_for_dev_db/superuser.py
    echo -e "\nEverything is done."
}


YELLOW='\033[1;33m'
NC='\033[0m' # No Color

printf "${NC}[ ${YELLOW}WARNING${NC} ] This script will destroy the entire database irreversibly!"
printf "\nBe sure that you run this script from the root of your app project.\nYou are currently in `pwd`"
echo
while true; do
    read -p "Are you sure you know what you are doing? [y/n] " yn
        case ${yn} in
	    [Yy]* ) reset_db; break;;
            [Nn]* ) exit;;
                * ) echo "Please answer yes or no.";;
        esac
done
