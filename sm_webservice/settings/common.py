import os
from pathlib import Path

# noinspection PyUnresolvedReferences,PyPackageRequirements
from openmm.unit import kelvin, femtoseconds, kilojoule, nanometer, mole, kilojoule_per_mole

# noinspection PyUnresolvedReferences
from .sm_version import *

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = Path(__file__).resolve().parent.parent.parent

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_celery_results',
    # 'debug_toolbar',
    'tasks',
    'static_pages',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',
]

ROOT_URLCONF = 'sm_webservice.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'tasks.context_processors.get_version',
            ],
        },
    },
]

WSGI_APPLICATION = 'sm_webservice.wsgi.application'

# Database

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# Password validation

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator', },
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator', },
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator', },
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator', },
]

# Internationalization
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static_dev"),
]

INTERNAL_IPS = ['127.0.0.1']

LOGGING_LEVEL = "DEBUG"


# Other Celery settings
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

N_CPUS = 10

##############################

MIN_NUMBER_OF_BEADS = 5
MAX_NUMBER_OF_BEADS = 2000
TASKS_FOLDER = os.path.join(BASE_DIR, 'data/tasks_data')
MAX_TASKS_TO_RUN = 4
FORCEFIELD_PATH = os.path.join(BASE_DIR, 'sm_engine', 'classic_sm_ff.xml')

# MODELING PARAMS

SPRING_LENGTH = 0.2  # nm
SPRING_STRENGTH = 300000.0
SPHERICAL_CONTAINER_SCALE = 10000
DIRE_FORCE = 50 * kilojoule_per_mole
NUMBER_OF_MIN_DYN_CYCLES = 5  # This strongly affects execution time
NUMBER_OF_DYN_STEPS = 1000
ROUGH_MIN_TOLERANCE = 10 * kilojoule/(nanometer*mole)
FINE_MIN_TOLERANCE = 1 * kilojoule/(nanometer*mole)
DYN_TEMPERATURE = 300 * kelvin
DYN_TIME_STEP = 20 * femtoseconds
SUSPICIOUS_ENERGY = 1000 * kilojoule_per_mole  # If energy will be higher than this param it will be reported to log as warning.
SAWR_MAXIMUM_TRIES = 10  # How many times do we try to generate SAWR initial structure until error is raised.
EV_EPSILON = 2.86
EV_SIGMA = 0.05
