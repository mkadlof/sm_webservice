from .common import *

DEBUG = True

SECRET_KEY = 'oy**&04&!ll&x16o)cgp&3g77ldplyaus=s*zl^2726^9@tz3b'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] [%(name)s:%(lineno)s] [%(levelname)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S",
        },
        'condole_for_debug': {
            'format': "[%(levelname)s] %(message)s [%(name)s:%(lineno)s] ",
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
        'log_file': {
            'class': 'logging.FileHandler',
            'filename': 'log/log',
            'formatter': 'standard',
        },
    },
    'loggers': {
        'sm_logger': {
            'handlers': ['log_file'],
            'level': 'INFO',
        },
    },
}

# Broker configuration using Redis
CELERY_BROKER_URL = 'redis://localhost:6379/0'  # URL to your Redis instance

# Celery result backend using Redis (opcjonalne, jeśli potrzebujesz)
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'  # URL to your Redis instance