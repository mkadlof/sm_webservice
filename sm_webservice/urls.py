from django.conf import settings
from django.urls import path
from django.conf.urls import include
from tasks.views import index

urlpatterns = [
    path('', index, name='index'),
    path('tasks/', include('tasks.urls')),
    path('', include('static_pages.urls'))
]

# if settings.DEBUG:
#     import debug_toolbar
#
#     urlpatterns = [
#                       url(r'^__debug__/', include(debug_toolbar.urls)),
#                   ] + urlpatterns
