#! /usr/bin/env python3

import argparse
import sys
from typing import Tuple

import numpy as np
from numpy import pi, sin, cos, sqrt

from .points_io import save_points_as_pdb


class FailedToBuildStructureError(Exception):
    pass


def random_versor() -> np.ndarray:
    """generate random versor"""
    x = np.random.uniform(-1, 1)
    y = np.random.uniform(-1, 1)
    z = np.random.uniform(-1, 1)
    d = (x ** 2 + y ** 2 + z ** 2) ** 0.5
    return np.array([x / d, y / d, z / d])


def random_versors(n: int) -> np.ndarray:
    """Generate vector of random versors."""
    points = np.random.uniform(-1, 1, size=(n, 3))
    points = points / np.linalg.norm(points, axis=1).reshape((n, 1))
    return points


def dist(p1: Tuple[float, float, float], p2: Tuple[float, float, float]) -> float:
    """Calculates distance between two points."""
    x1, y1, z1 = p1
    x2, y2, z2 = p2
    return ((x1 - x2) ** 2 + (y1 - y2) ** 2 + (z1 - z2) ** 2) ** 0.5


def line(n: int, displacement: float = .0) -> np.ndarray:
    """Almost straight line"""
    points = []
    for i in range(n):
        points.append([i, 0, 0])
    points = np.array(points)
    if displacement == .0:
        return points
    else:
        displacement = displacement * random_versors(n)
        points = points + displacement
        return points


def polymer_circle(n, z_stretch=0) -> np.ndarray:
    points = []
    angle_increment = 360 / float(n)
    radius = 1 / (2 * sin(np.radians(angle_increment) / 2.))  # assure distance 1
    z = 0
    for i in range(n):
        x = radius * cos(angle_increment * i * pi / 180)
        y = radius * sin(angle_increment * i * pi / 180)
        if z_stretch != 0:
            z += z_stretch
        points.append((x, y, z))
    return np.array(points)


def random_walk(n: int) -> np.ndarray:
    versors = random_versors(n-1)
    points = [[0, 0, 0]]
    for i in versors:
        points.append([points[-1][0] + i[0], points[-1][1] + i[1], points[-1][2] + i[2]])
    return np.array(points)


def self_avoiding_random_walk(n: int, step=1, bead_radius=0.5, epsilon=0.001, total_count=0, limit=10) -> np.ndarray:
    if total_count > limit:
        raise FailedToBuildStructureError(f"More than {limit} tries of generating initial SAWR.")
    points = np.array([[0, 0, 0]])
    epsilon_dist = (2 * bead_radius - epsilon) ** 2
    potential_new_step = None
    for i in range(n - 1):
        step_is_ok = False
        count = 0
        while not step_is_ok:
            potential_new_step = points[-1] + step * random_versor()
            if not np.any(np.sum((points - potential_new_step) ** 2, axis=1) < epsilon_dist):
                step_is_ok = True
            else:
                count += 1
                if count > 100:
                    return self_avoiding_random_walk(n, step, bead_radius, epsilon, total_count + 1, limit)
        points = np.vstack((points, potential_new_step))
    return points


def spiral_sphere(n: int, r, c) -> np.ndarray:
    """based on: http://elib.mi.sanu.ac.rs/files/journals/vm/57/vmn57p2-10.pdf"""

    def calc_x(t): return sqrt(r ** 2 - t ** 2) * cos(t / c)

    def calc_y(t): return sqrt(r ** 2 - t ** 2) * sin(t / c)

    def calc_z(t): return t

    t_lin = np.linspace(-r, r, n)
    x = calc_x(t_lin).reshape((n, 1))
    y = calc_y(t_lin).reshape((n, 1))
    z = calc_z(t_lin).reshape((n, 1))
    points = np.concatenate((x, y, z), 1)
    return points


def baseball(n: int, r: float) -> np.ndarray:
    """based on: http://paulbourke.net/geometry/baseball/"""
    b = pi / 2
    a = 0.4  # shape parameter. Better not to change

    def calc_x(t): return r * sin(b - (b - a) * cos(t)) * cos(t / 2. + a * sin(2 * t))

    def calc_y(t): return r * sin(b - (b - a) * cos(t)) * sin(t / 2. + a * sin(2 * t))

    def calc_z(t): return r * cos(b - (b - a) * cos(t))

    t_lin = np.linspace(0, 4 * pi - (4 * pi) / n, n)
    x = calc_x(t_lin).reshape((n, 1))
    y = calc_y(t_lin).reshape((n, 1))
    z = calc_z(t_lin).reshape((n, 1))
    points = np.concatenate((x, y, z), 1)
    return points


def random_gas(n: int, d: float) -> np.ndarray:
    x = np.random.uniform(-d, d, n).reshape((n, 1))
    y = np.random.uniform(-d, d, n).reshape((n, 1))
    z = np.random.uniform(-d, d, n).reshape((n, 1))
    points = np.concatenate((x, y, z), axis=1)
    return points


def center(points: np.ndarray, new_center: tuple) -> np.ndarray:
    """
    Shifts points to new mass center.

    :param points: 3xN np.array of points
    :param new_center: 3 floats tuple position of new mass center
    :return: shifted points
    """
    new_center = np.array(new_center)
    center_before_centering = np.mean(points, axis=0)
    centering_vector = new_center - center_before_centering
    points = points + centering_vector
    return points


def build(args):
    if args.N > 9999:
        sys.exit("9999 beads is the limit! You requested for {}".format(args.N))
    error_msg = "Wrong number of parameters for {{}}. Please consult {0} -h".format(sys.argv[0])
    render_connect = True  # default value for save_points_as_pdb()
    if args.type == "line":
        if len(args.params) != 0:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        points = line(args.N, displacement=0.1)

    elif args.type == "circle":
        if len(args.params) != 0:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        z_stretch = args.params[0]
        points = polymer_circle(args.N, z_stretch)

    elif args.type == "spiral_sphere":
        if len(args.params) != 2:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        r = args.params[0]
        c = args.params[1]
        points = spiral_sphere(args.N, r, c)

    elif args.type == "baseball":
        if len(args.params) != 1:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        r = args.params[0]
        points = baseball(args.N, r)

    elif args.type == "random_gas":
        if len(args.params) != 1:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        d = args.params[0]
        points = random_gas(args.N, d)
        render_connect = False

    elif args.type == "random_walk":
        if len(args.params) != 0:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        points = random_walk(args.N)

    elif args.type == "self_avoiding_random_walk":
        if len(args.params) != 0:
            sys.exit(error_msg.format(args.type))
        points = self_avoiding_random_walk(args.N, step=args.step, bead_radius=args.step / 2)

    else:
        print("Wrong type {}. Please consult {} -h".format(args.type, sys.argv[0]))
        sys.exit(1)

    if args.center:
        points = center(points, (0., 0., 0.))
    save_points_as_pdb(points, args.output, save_psf=args.psf, render_connect=render_connect, verbose=False)


def main():
    longer_help = """
    Additional params:
    line:
        no extra args
    circle
        z - z stretch (float)
    spiral_sphere:
        r - radius of sphere (float)
        c - controls number of turns (float)
    baseball:
        r - radius of sphere (float)
    random_gas:
        d - dimension of box (-d, d)
    random_walk:
        no extra args
    self_avoiding_random_walk
        no extra args
    """

    parser = argparse.ArgumentParser(description="Generate initial structure for polymer MD simulations",
                                     epilog=longer_help, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-c", '--center', action="store_true", help="Center in PDB box? [4500, 4500, 4500]")
    parser.add_argument("-o", '--output', default="initial_structure.pdb", help="output PDB file name")
    parser.add_argument("-p", '--psf', action="store_true", help="generate PSF file")
    parser.add_argument("-s", '--step', type=float, default=1, help="step size (default: 1)")
    parser.add_argument("type", help="circle|line|spiral-sphere|baseball|random_gas")
    parser.add_argument("N", type=int, help="number of beads")
    parser.add_argument("params", metavar='params', type=float, nargs='*',
                        help="Additional params type dependent params")
    args = parser.parse_args()
    build(args)


if __name__ == '__main__':
    main()
