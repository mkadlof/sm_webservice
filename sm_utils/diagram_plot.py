import matplotlib

matplotlib.use('Agg')

# noinspection PyPep8
from matplotlib import pyplot as plt
# noinspection PyPep8
from matplotlib.patches import Ellipse


def make_arcs_diagram_plot(task):
    restraints = [list(i) for i in task.get_restraints()]
    plt.clf()
    fig, ax = plt.subplots(figsize=(7, 2))
    max_width = 0
    for rst in restraints:
        xy = ((rst[1] - rst[0]) / 2 + rst[0], 0)
        width = rst[1] - rst[0]
        max_width = max(max_width, width)
        e = Ellipse(xy=xy, width=width, height=width, facecolor='#15b01a10', edgecolor='#15b01a', linewidth=1.5)
        ax.add_artist(e)
    ax.get_yaxis().set_visible(False)
    ax.set_xlim(1, task.number_of_beads)
    if len(restraints) > 0:
        ax.set_ylim(0, 0.5 * max_width * 1.1)
    else:
        plt.text(0.5, 0.5, 'No restraints defined', horizontalalignment='center', verticalalignment='center',
                 transform=ax.transAxes)
    plt.savefig(task.get_plot_arcs_path(), bbox_inches='tight', pad_inches=0.1)
