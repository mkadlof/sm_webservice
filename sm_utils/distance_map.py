import matplotlib

matplotlib.use('Agg')

# noinspection PyPep8
from matplotlib import pyplot as plt
# noinspection PyPep8
from scipy.spatial.distance import pdist, squareform

# noinspection PyPep8
from .points_io import point_reader
# noinspection PyPep8
from tasks.models import Task


def make_distance_map(task: Task) -> None:
    points = point_reader(task.get_output_path())
    distances = squareform(pdist(points))
    plt.clf()
    n = task.number_of_beads
    plt.imshow(distances, interpolation='none', origin='lower', cmap='plasma_r', aspect='equal', extent=(1, n, 1, n))
    plt.colorbar()
    plt.savefig(task.get_distance_map_path(), bbox_inches='tight', pad_inches=0.1)
