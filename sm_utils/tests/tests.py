import os
import shutil
import sys
import tempfile
import unittest

import numpy as np
from django.conf import settings
from django.test import TestCase

from .. import generator as g
from ..points_io import point_reader


class GeneratorTests(TestCase):
    TESTS_DATA = os.path.join(settings.BASE_DIR, 'sm_utils/tests/tests_data')

    def _test_data_dir_exist(self):
        return os.path.isdir(self.TESTS_DATA)

    def setUp(self):
        self.test_dir = tempfile.mkdtemp()
        if not self._test_data_dir_exist():
            sys.exit("Directory with data used for tests is missing. Tests will not be run.")

    def tearDown(self):
        shutil.rmtree(self.test_dir)

    def test_rw(self):
        """Test if final structure have valid number of beads."""
        tested_file = os.path.join(self.test_dir, 'test.pdb')

        class Args(object):
            N = 10
            center = False
            output = tested_file
            params = []
            type = 'random_walk'
            psf = False
            step = 1

        g.build(Args)
        structure = point_reader(tested_file)
        self.assertEqual(len(structure), Args.N)

    def test_sarw(self):
        """Test if final structure have valid number of beads."""
        tested_file = os.path.join(self.test_dir, 'test.pdb')

        class Args(object):
            N = 10
            center = False
            output = tested_file
            params = []
            type = 'self_avoiding_random_walk'
            psf = False
            step = 1

        g.build(Args)
        structure = point_reader(tested_file)
        self.assertEqual(len(structure), Args.N)

    def test_spiral_sphere(self):
        tested_file = os.path.join(self.test_dir, 'test.pdb')
        gold_file = os.path.join(self.TESTS_DATA, 'test_spiral_sphere.pdb')

        class Args(object):
            N = 100
            center = False
            output = tested_file
            params = [10.0, 1.0]
            type = 'spiral_sphere'
            psf = False
            step = 1

        g.build(Args)
        with open(tested_file) as f1, open(gold_file) as f2:
            t1 = f1.read()
            t2 = f2.read()
        self.assertMultiLineEqual(t1, t2)

    def test_baseball(self):
        tested_file = os.path.join(self.test_dir, 'test.pdb')
        gold_file = os.path.join(self.TESTS_DATA, 'test_baseball.pdb')

        class Args(object):
            N = 100
            center = False
            output = tested_file
            params = [10.0]
            type = 'baseball'
            psf = False
            step = 1

        g.build(Args)
        with open(tested_file) as f1, open(gold_file) as f2:
            t1 = f1.read()
            t2 = f2.read()
        self.assertMultiLineEqual(t1, t2)

    def test_gas_does_not_contain_connect_records(self):
        tested_file = os.path.join(self.test_dir, 'test.pdb')

        class Args(object):
            N = 1000
            center = False
            output = tested_file
            params = [50.0]
            type = 'random_gas'
            psf = False
            step = 1

        g.build(Args)
        with open(tested_file) as f:
            t = f.read()
        self.assertNotIn('CONECT', t)

    def test_psf_file_is_created_in_line_case(self):
        tested_file = os.path.join(self.test_dir, 'test.pdb')

        class Args(object):
            N = 2
            center = False
            output = tested_file
            params = []
            type = 'line'
            psf = True
            step = 1

        g.build(Args)
        expected_file_name = os.path.join(self.test_dir, 'test.psf')
        assert os.path.isfile(expected_file_name)

    def test_psf_file_is_correct(self):
        pdb_file = os.path.join(self.test_dir, 'test.pdb')
        tested_file = os.path.join(self.test_dir, 'test.psf')
        gold_file = os.path.join(self.TESTS_DATA, 'test_psf_dimer.psf')

        class Args(object):
            N = 2
            center = False
            output = pdb_file
            params = []
            type = 'line'
            psf = True
            step = 1

        g.build(Args)
        with open(tested_file) as f1, open(gold_file) as f2:
            t1 = f1.read()
            t2 = f2.read()
        self.assertMultiLineEqual(t1, t2)

    def test_centering(self):
        test_points = np.arange(12).reshape([4, 3])
        points_after_centering = g.center(test_points, (4500., 4500., 4500.))
        after_centering = np.mean(points_after_centering)
        self.assertTrue((after_centering == np.array([4500, 4500, 4500])).all())

    def test_random_versor_is_length_one(self):
        x, y, z = g.random_versor()
        self.assertAlmostEqual((x ** 2 + y ** 2 + z ** 2), 1)

    def test_versors(self):
        n = 10
        points = g.random_versors(n)
        self.assertTrue(np.allclose(np.linalg.norm(points, axis=1), np.ones(n)))


if __name__ == '__main__':
    unittest.main()
