Logwatch
--------

#### Whats this?


Content of this folder is not strictly part of an Spring-Model webservice. This is plugin and example of config files for logwatch. An log analyzing tool. Logwatch allows you to receive periodic email reports, about some key services in your system.

This versions supports only logs produced by Celery workers.

#### Requirements

- Logwatch 7.4.2
- Ubuntu 16.04 LTS

#### Files

Logwatch requiere 3 files:
 
- logfiles - group of log files to process
- service - definition of service and connection with logfiles group
- script - actual parser to logs

In the headers of each of file is information whee to put them. File names are important. Do not change them. Since the names of files are similar, the had to be put in separate directories.

##### TODO

It would be nice to add modify logs, and filter to make it possible to distinct between own requests (i.e. from CeNT - filtered by whitlisted IP's, and other people requests.)