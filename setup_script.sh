#!/bin/bash

# This script will install everything that is needed for fresh setup of sm_wbservice on
# fresh ubuntu 16.04 Ubuntu Machine. It must be run without sudo although sudo command is called inside.

# MODIFY ONLY IF YOU KNOW WHAT ARE YOU DOING
PYTHON_VERSION=3.7.1
OPENMM_VERSION=7.3.0
VENV_NAME=sm_webservice_venv
OPENMM_SRC_DIR=/tmp/openmm
OPENMM_BUILD_DIR=${OPENMM_SRC_DIR}/build

# DO NOT EDIT FOLLOWING
PYENV_ROOT=$HOME/.pyenv
PYTHON=${PYENV_ROOT}/versions/${VENV_NAME}/bin/python
OPENMM_INSTALL_DIR=${PYENV_ROOT}/versions/${VENV_NAME}/lib/python${PYTHON_VERSION:0:3}/site-packages/openmm

# ======================================
#                  Pyenv
# ======================================

echo Checking pyenv installation...
if ! [[ -x "$(command -v pyenv)" ]]; then
    curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
    export PATH="/home/michal/.pyenv/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

# ======================================
#    Python and OpenMM dependencies
# ======================================

echo Checking dependencies...
PACKAGES="
make
build-essential
libssl-dev
zlib1g-dev
libbz2-dev
libreadline-dev
libsqlite3-dev
wget
curl
llvm
libncurses5-dev
libncursesw5-dev
xz-utils
tk-dev
libffi-dev
liblzma-dev
python-openssl
clang
cmake
git
swig
doxygen"

for PKG in ${PACKAGES}; do
    if [[ $(dpkg-query -W -f='${Status}' ${PKG} 2>/dev/null | grep -c "ok installed") -eq 0 ]]; then
        sudo apt -y install ${PKG};
    fi
done

# For MacOS you can use following
#
# brew install make wget curl llvm cmake git swig doxygen

# ======================================
#          Python virtual env
# ======================================

echo Setting up virtual env...
pyenv virtualenv-delete -f ${VENV_NAME}

if ! pyenv versions | grep -q ${PYTHON_VERSION}; then
    echo "Installing python ${PYTHON_VERSION}"
    pyenv install ${PYTHON_VERSION}
fi

pyenv virtualenv ${PYTHON_VERSION} ${VENV_NAME}
pyenv shell ${VENV_NAME}
pip install --upgrade pip
pip install --upgrade setuptools
pip install -r requirements.txt

# ==========================================================
# Download, compile and install minimal OpenMM tuned for CPU
# ==========================================================

echo Compiling OpenMM...
#rm -rf ${OPENMM_SRC_DIR} 2> /dev/null
if [[ ! -d ${OPENMM_SRC_DIR} ]]; then
    git clone https://github.com/pandegroup/openmm.git ${OPENMM_SRC_DIR}
fi
cd ${OPENMM_SRC_DIR}
git checkout ${OPENMM_VERSION}

rm -rf ${OPENMM_BUILD_DIR} 2> /dev/null
rm -rf ${OPENMM_INSTALL_DIR} 2> /dev/null

mkdir ${OPENMM_BUILD_DIR}
cd ${OPENMM_BUILD_DIR}

make clean

cmake -D BUILD_TESTING:BOOL=OFF\
 -D CMAKE_BUILD_TYPE:STRING=Release\
 -D CMAKE_INSTALL_PREFIX:PATH=${OPENMM_INSTALL_DIR}\
 -D CUDA_HOST_COMPILER:FILEPATH=/usr/bin/cc\
 -D CUDA_SDK_ROOT_DIR:PATH=CUDA_SDK_ROOT_DIR-NOTFOUND\
 -D CUDA_TOOLKIT_ROOT_DIR:PATH=CUDA_TOOLKIT_ROOT_DIR-NOTFOUND\
 -D CUDA_USE_STATIC_CUDA_RUNTIME:BOOL=OFF\
 -D CUDA_nvrtc_LIBRARY:FILEPATH=CUDA_nvrtc_LIBRARY-NOTFOUND\
 -D DOXYGEN_EXECUTABLE:FILEPATH=/usr/bin/doxygen\
 -D OPENMM_BUILD_AMOEBA_CUDA_LIB:BOOL=OFF\
 -D OPENMM_BUILD_AMOEBA_PLUGIN:BOOL=ON\
 -D OPENMM_BUILD_CPU_LIB:BOOL=ON\
 -D OPENMM_BUILD_CUDA_COMPILER_PLUGIN:BOOL=OFF\
 -D OPENMM_BUILD_CUDA_LIB:BOOL=OFF\
 -D OPENMM_BUILD_C_AND_FORTRAN_WRAPPERS:BOOL=OFF\
 -D OPENMM_BUILD_DRUDE_CUDA_LIB:BOOL=OFF\
 -D OPENMM_BUILD_DRUDE_OPENCL_LIB:BOOL=OFF\
 -D OPENMM_BUILD_DRUDE_PLUGIN:BOOL=ON\
 -D OPENMM_BUILD_EXAMPLES:BOOL=OFF\
 -D OPENMM_BUILD_OPENCL_LIB:BOOL=OFF\
 -D OPENMM_BUILD_PME_PLUGIN:BOOL=OFF\
 -D OPENMM_BUILD_PYTHON_WRAPPERS:BOOL=ON\
 -D OPENMM_BUILD_RPMD_CUDA_LIB:BOOL=OFF\
 -D OPENMM_BUILD_RPMD_OPENCL_LIB:BOOL=OFF\
 -D OPENMM_BUILD_RPMD_PLUGIN:BOOL=ON\
 -D OPENMM_BUILD_SHARED_LIB:BOOL=ON\
 -D OPENMM_BUILD_STATIC_LIB:BOOL=OFF\
 -D OPENMM_GENERATE_API_DOCS:BOOL=OFF\
 -D PYTHON_EXECUTABLE:FILEPATH=${PYTHON}\
 -D SWIG_EXECUTABLE:FILEPATH=/usr/bin/swig ..

make -j `nproc`
make install
make PythonInstall

echo Cleaning up...
rm -rf ${OPENMM_SRC_DIR}

echo Testing...
if ${PYTHON} -c 'from simtk import openmm' 2> /dev/null; then
    echo Everything is OK
else
    echo Setup failed.
    exit 1
fi
