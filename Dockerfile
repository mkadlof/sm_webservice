FROM mkadlof/openmm:8.0 as builder-spring-model

RUN --mount=target=/var/lib/apt/lists,type=cache,sharing=locked \
    --mount=target=/var/cache/apt,type=cache,sharing=locked \
    rm -f /etc/apt/apt.conf.d/docker-clean \
    && apt update \
    && apt -y --no-install-recommends install python3.10-venv build-essential python3-dev \
    && python3 -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
COPY requirements.txt requirements.txt
RUN pip install --upgrade pip && \
    pip install -r requirements.txt


FROM mkadlof/openmm:8.0 as spring-model
RUN --mount=target=/var/lib/apt/lists,type=cache,sharing=locked \
    --mount=target=/var/cache/apt,type=cache,sharing=locked \
    rm -f /etc/apt/apt.conf.d/docker-clean \
    && apt update \
    && apt -y --no-install-recommends install python3-distutils python3-dev

WORKDIR /home/sm_webservice
ENV PATH="/opt/venv/bin:$PATH"
ENV DJANGO_SETTINGS_MODULE=sm_webservice.settings
COPY --from=builder-spring-model /opt /opt
EXPOSE 5000
COPY . .
RUN chown -R 33:33 /home/sm_webservice/data/tasks_data
