import re

from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


def is_number(s: str) -> bool:
    try:
        float(s)
        return True
    except ValueError:
        return False


@stringfilter
def format_thousands(val: str) -> str:
    """Format a number so that it's thousands are separated by space.
    1000000 > '1 000 000'
    10000 > '10 000'
    10000.00 > '10 000.00'"""
    if is_number(val):
        return ' '.join(re.findall(r'((?:\d+\.)?\d{1,3})', val[::-1]))[::-1]
    else:
        return val


register.filter('format_thousands', format_thousands)
