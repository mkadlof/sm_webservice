from django.urls import path

from . import views

urlpatterns = [
    path('list', views.TasksListView.as_view(), name='list_of_tasks'),
    path('<uuid:pk>/initial_structure.pdb', views.serve_initial_structure),
    path('<uuid:pk>/output.pdb', views.serve_minimized_structure),
    path('<uuid:pk>/restraints.rst', views.serve_restraints),
    path('<uuid:pk>/arcs.png', views.serve_arcs_diagram),
    path('<uuid:pk>/distance_map.png', views.serve_distance_map),
    path('<uuid:pk>', views.TasksDetailView.as_view(), name='task_details'),
]
