import logging
import threading as th
from math import floor
from queue import Queue, Empty

from django.conf import settings
from django.core.management.base import BaseCommand
from psutil import cpu_count, cpu_percent

from sm_engine.tasks import run_sm
from sm_utils.diagram_plot import make_arcs_diagram_plot
from sm_utils.distance_map import make_distance_map
from tasks.models import Task

log = logging.getLogger('sm_logger')


def do_task(tasks: Queue, signals: Queue) -> None:
    while True:
        try:
            task = tasks.get(block=False)
        except Empty:
            break
        signals.put((task, 'R'))
        try:
            run_sm(task)
            make_distance_map(task)
            make_arcs_diagram_plot(task)
        except Exception as err:
            signals.put((task, 'E'))
            log.error(f'{str(task.id)} | {err}')
            log.exception(err)
        signals.put((task, 'F'))


class Command(BaseCommand):
    help = "Runs the tasks."

    @staticmethod
    def get_max_number_of_task_to_run():
        if settings.MAX_TASKS_TO_RUN == 0:
            cpus_available = cpu_count()
        else:
            cpus_available = min(cpu_count(), settings.MAX_TASKS_TO_RUN)
        free_cpus = floor(cpu_count() - cpu_count() * cpu_percent() / 100)
        return min(free_cpus, cpus_available)

    def handle(self, *args, **options):
        waiting_tasks = Task.objects.filter(status='W').order_by('timestamp')

        if len(waiting_tasks) == 0:
            # There aren't any tasks to run
            return
        else:
            available_number_of_threads = self.get_max_number_of_task_to_run()
            tasks_queue = Queue()
            signals_queue = Queue()

            for task in waiting_tasks:
                tasks_queue.put(task)

            threads = []
            for i in range(min(available_number_of_threads, len(waiting_tasks))):
                t = th.Thread(target=do_task, args=(tasks_queue, signals_queue))
                t.start()
                threads.append(t)

            # TODO: Greg told this is dumb solution. It's his solution... but still works. Not sure, why...
            for _ in range(len(waiting_tasks) * 2):
                task, signal = signals_queue.get()
                if signal == 'R':
                    task.set_status_running()
                elif signal == 'F':
                    task.set_status_finished()
                elif signal == 'E':
                    task.set_status_error()
                else:
                    raise ValueError('Invalid status received.')

            # stopping workers
            for t in threads:
                t.join()
