from django.apps import AppConfig


class RetrieveRequestConfig(AppConfig):
    name = 'tasks'
