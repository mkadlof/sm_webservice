from django.test import TestCase

from ..models import BedpeRecord, Region, Task


class BedpeRecordsTests(TestCase):

    def setUp(self):
        self.r = BedpeRecord(chrom1="1", begin1=4, end1=8, chrom2="1", begin2=10, end2=13, value=10)
        self.task = Task.objects.create(initial_structure_type=1, number_of_beads=10)

    def tearDown(self):
        self.task.delete()

    def test_anchors(self):
        self.assertEqual(self.r.begin, 6)
        self.assertEqual(self.r.end, 11)

    def test_bedpe_record_str(self):
        self.assertEqual(str(self.r), 'chr1	4	8	chr1	10	13	10')

    def test_wrong_coordinates_raises_exception(self):
        r = BedpeRecord(chrom1="1", begin1=10, end1=8, chrom2="chr1", begin2=12, end2=20, value=10, task=self.task)
        self.assertRaises(ValueError, r.save)


class RegionTests(TestCase):
    def setUp(self):
        self.task = Task.objects.create(initial_structure_type=1, number_of_beads=10)

    def tearDown(self):
        self.task.delete()

    def test_region_str(self):
        r = Region(chromosome="1", begin="10", end="20")
        self.assertEqual(str(r), 'chr1:10-20')

    def test_region_chr_normalization(self):
        r = Region(chromosome="1", begin="10", end="20", task=self.task)
        r.save()
        self.assertEqual(r.chromosome, '1')

    def test_wrong_coords_region(self):
        t = Task()
        r = Region(chromosome="1", begin="20", end="10", task=t)
        self.assertRaises(ValueError, r.save)
