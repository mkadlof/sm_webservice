from django.test import TestCase

from ..miscellanea_functions import mid_point, translate_interactions_to_restraints, find_limits_in_interactions
from ..templatetags.app_extras import format_thousands


class MiscellaneaTests(TestCase):
    def test_midpoint(self):
        self.assertEqual(mid_point(5, 8), 6)
        self.assertEqual(mid_point(100, 200), 150)

    def test_transform(self):
        test_input = [('chr7', 950, 1050, 'chr7', 1950, 2050, 5, "N"),
                      ('chr7', 950, 1050, 'chr7', 2950, 3050, 5, "N"),
                      ('chr7', 3950, 4050, 'chr7', 4950, 5050, 5, "N"),
                      ('chr7', 6010, 6020, 'chr7', 6040, 6050, 5, "N")]
        expected_output = [(2, 4, "N"), (2, 6, "N"), (8, 10, "N")]
        actual_output = translate_interactions_to_restraints(interactions=test_input, resolution=500, begin=1)
        self.assertEqual(expected_output, actual_output)

    def test_transform2(self):
        test_input = [('chr7', 3004450, 3004550, 'chr7', 3007450, 3007550, 5, "N"),
                      ('chr7', 3004750, 3004850, 'chr7', 3007150, 3007250, 5, "N")]
        expected_output = [(5, 8, "N")]
        actual_output = translate_interactions_to_restraints(interactions=test_input, resolution=1000, begin=3000000)
        self.assertEqual(expected_output, actual_output)

    def test_transform3(self):
        """None of the result bead indexes can be lower than 1. It's a bug fix test, that resolves particular
         data input. It may look a little wired but it's valid."""
        test_input = [('chr1', 25566356, 25566679, 'chr1', 26136209, 26138021, 4, 'N')]
        actual_output = translate_interactions_to_restraints(interactions=test_input, resolution=2000, begin=25566356)
        self.assertTrue(actual_output[0][0] >= 1)

    def test_transform4(self):
        test_input = [('chr1', 1, 1, 'chr1', 1001, 1001, 4, 'N')]
        expected_output = [(1, 3, "N")]
        actual_output = translate_interactions_to_restraints(interactions=test_input, resolution=500, begin=1)
        self.assertEqual(expected_output, actual_output)

    def test_transform5(self):
        test_input = [('chr1', 500, 500, 'chr1', 1200, 1202, 4, 'N')]
        expected_output = [(1, 3, "N")]
        actual_output = translate_interactions_to_restraints(interactions=test_input, resolution=500, begin=1)
        self.assertEqual(expected_output, actual_output)

    def test_find_limits_in_interactions(self):
        test_input = [('chr7', 950, 1050, 'chr7', 1950, 2050, 5, "N"),
                      ('chr7', 950, 1050, 'chr7', 2950, 3050, 5, "N"),
                      ('chr7', 3950, 4050, 'chr7', 4950, 5050, 5, "N"),
                      ('chr7', 6010, 6020, 'chr7', 6040, 6050, 5, "N")]
        expected_output = 'chr7', 950, 6050
        actual_output = find_limits_in_interactions(test_input)
        self.assertEqual(expected_output, actual_output)


class TemplateTagsTests(TestCase):
    def test_format_thousands_1(self):
        self.assertEqual(format_thousands('100'), '100')
        self.assertEqual(format_thousands('1000'), '1 000')
        self.assertEqual(format_thousands('1.2'), '1.2')
        self.assertEqual(format_thousands('1000.0'), '1 000.0')
