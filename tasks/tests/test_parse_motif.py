from django.test import TestCase
from django.core.exceptions import ValidationError

from ..forms import NewTaskForm


class TestNewTaskForm(TestCase):

    def setUp(self):
        self.form = NewTaskForm()

    def test_parse_motif(self):
        self.assertEqual(self.form._parse_motif('<>'), 'H')
        self.assertEqual(self.form._parse_motif('><'), 'H')
        self.assertEqual(self.form._parse_motif('>>'), 'C')
        self.assertEqual(self.form._parse_motif('<<'), 'C')
        self.assertEqual(self.form._parse_motif(None), "N")
        self.assertEqual(self.form._parse_motif(''), "N")
        self.assertRaises(ValidationError, self.form._parse_motif, ['dupa'])
