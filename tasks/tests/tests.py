import logging
import os

from django.conf import settings
from django.test import TestCase, Client
from django.test.utils import override_settings

from sm_engine.tasks import run_sm
from ..models import Task

logging.disable(logging.CRITICAL)


@override_settings(CELERY_TASK_EAGER_PROPAGATES=True,
                   CELERY_TASK_ALWAYS_EAGER=True)
class BasicTests(TestCase):
    """Test if every site response."""

    def test_index_response(self):
        url = '/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_send_new_task_model_coords(self):
        url = '/'
        data = {
            'title': 'Test tasks',
            'initial_structure_type': '1',
            'number_of_beads': '300',
            'stiffness': '10',
            'restraints': '10\t20',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        Task.objects.get(title='Test tasks').delete()

    def test_send_new_task_ideal_chain(self):
        url = '/'
        data = {
            'title': 'Test tasks',
            'initial_structure_type': '2',
            'number_of_beads': '300',
            'stiffness': '0',
            'excluded_volume': 'off'
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        Task.objects.get(title='Test tasks').delete()

    def test_send_new_task_model_coords_spherical_container(self):
        url = '/'
        data = {
            'title': 'Test tasks',
            'initial_structure_type': '1',
            'number_of_beads': '300',
            'stiffness': '10',
            'restraints': '10\t20',
            'spherical_container': 10,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        Task.objects.get(title='Test tasks').delete()

    def test_send_new_task_genomic_coords(self):
        url = '/'
        data = {
            'title': 'Test tasks',
            'initial_structure_type': '1',
            'region': 'chr7:141250000-141850000',
            'resolution': '2000',
            'stiffness': '10',
            'interactions': 'chr7\t141319306\t141319888\tchr7\t141502227\t141502862\t7\r\n'
                            'chr7\t141439952\t141440562\tchr7\t141475636\t141476247\t4\r\n'
                            'chr7\t141440408\t141441058\tchr7\t141540288\t141540817\t4\r\n'
                            'chr7\t141521373\t141522026\tchr7\t141649615\t141650144\t4\r\n'
                            'chr7\t141532718\t141534198\tchr7\t141648797\t141650013\t6\r\n'
                            'chr7\t141532773\t141533502\tchr7\t141650079\t141651001\t4\r\n'
                            'chr7\t141533511\t141534645\tchr7\t141650062\t141651061\t4\r\n'
                            'chr7\t141535611\t141537022\tchr7\t141649504\t141650652\t5\r\n'
                            'chr7\t141655299\t141655869\tchr7\t141693222\t141693832\t5\r\n'
                            'chr7\t141653441\t141654793\tchr7\t141709665\t141711118\t9\r\n'
                            'chr7\t141656215\t141657786\tchr7\t141710387\t141711077\t4\r\n'
                            'chr7\t141653134\t141655804\tchr7\t141736601\t141739391\t40\r\n'
                            'chr7\t141655948\t141657565\tchr7\t141737637\t141738765\t4\r\n'
                            'chr7\t141652717\t141656457\tchr7\t141783888\t141787090\t43\r\n'
                            'chr7\t141656940\t141657491\tchr7\t141792255\t141792903\t8\r\n'
                            'chr7\t141701006\t141702162\tchr7\t141737490\t141738745\t9\r\n'
                            'chr7\t141737478\t141738238\tchr7\t141785355\t141786856\t5\r\n'
                            'chr7\t141754144\t141756309\tchr7\t141785377\t141787018\t8',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        Task.objects.get(title='Test tasks').delete()

    def test_send_new_task_loop_types(self):
        url = '/'
        data = {
            'title': 'Example - Loop types',
            'initial_structure_type': '0',
            'number_of_beads': '90',
            'stiffness': '10',
            'restraints': '10 40 >>\r\n50 80 ><',
            'excluded_volume': 'on',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        Task.objects.get(title='Example - Loop types').delete()

    def test_send_new_task_refinement(self):
        url = '/'
        data = {
            'title': 'Example - Loop types',
            'initial_structure_type': '1',
            'number_of_beads': '10',
            'stiffness': '10',
            'excluded_volume': 'on',
            'refinement': 'on'
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        Task.objects.get(title='Example - Loop types').delete()

    def test_list_of_tasks(self):
        url = '/tasks/list'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class TestUtils(object):
    next_task_id = 1

    @staticmethod
    def create_example_task():
        c = Client()
        example_task_title = f'Example task {TestUtils.next_task_id}'
        post_data = dict(title=example_task_title, number_of_beads='100',
                         restraints='10\t30\n10\t50\n10\t70\n10\t90\n30\t50\n30\t70\n30\t90\n50\t70\n50\t90\n70\t90\n',
                         initial_structure_type='1', stiffness='10')
        response = c.post('/', post_data)
        assert response.status_code == 200
        TestUtils.next_task_id += 1
        return Task.objects.get(title=example_task_title)


class TaskDeletion(TestCase):
    """Test of task deletion."""

    def test_on_task_deletion_delete_also_its_directory(self):
        test_task = TestUtils.create_example_task()
        path_to_be_deleted = test_task.get_path()
        test_task.delete()
        self.assertEqual(os.path.isdir(path_to_be_deleted), False)


class SingleTaskTests(TestCase):
    """Tests, that can be run on a single task"""

    task = None

    @classmethod
    def setUpClass(cls):
        cls.task = TestUtils.create_example_task()

    @classmethod
    def tearDownClass(cls):
        cls.task.delete()

    def test_task_have_initial_structure(self):
        self.assertTrue(os.path.isfile(SingleTaskTests.task.get_initial_structure_path()))

    def test_task_have_restraints_structure(self):
        self.assertTrue(os.path.isfile(SingleTaskTests.task.get_restraints_openmm_path()))


class TaskTests(TestCase):
    """Tests that require setting up a task, and removing it after test.

        All this tests require confidence that we can create and delete task properly.
    """

    def setUp(self):
        self.test_task = TestUtils.create_example_task()

    def tearDown(self):
        self.test_task.delete()

    def test_creation_of_new_task_creates_new_directory(self):
        task_directory = os.path.join(settings.TASKS_FOLDER, str(self.test_task.id))
        self.assertEqual(os.path.isdir(task_directory), True)

    def test_task_path_method(self):
        expected_task_path = os.path.join(settings.TASKS_FOLDER, str(self.test_task.id))
        self.assertEqual(expected_task_path, self.test_task.get_path())
        self.assertEqual(os.path.isdir(self.test_task.get_path()), True)

    def test_task_detail_view(self):
        self.assertEqual(os.path.isdir(self.test_task.get_path()), True)
        response = self.client.get(f'/tasks/{str(self.test_task.id)}')
        self.assertEqual(response.status_code, 200)


class RunTask(TestCase):
    """Test if task can be run"""

    def setUp(self):
        self.task = Task.objects.create(initial_structure_type=1, number_of_beads=10)

    def tearDown(self):
        self.task.delete()

    @override_settings(CELERY_TASK_EAGER_PROPAGATES=True,
                       CELERY_TASK_ALWAYS_EAGER=True)
    def test_task_is_proceded(self):
        result = run_sm.delay(self.task.id)
        t = Task.objects.get(id=self.task.id)
        self.assertEqual(t.status, 'F')
        self.assertTrue(result.state == 'SUCCESS')


@override_settings(CELERY_TASK_EAGER_PROPAGATES=True,
                   CELERY_TASK_ALWAYS_EAGER=True)
class AfterTaskExecution(TestCase):

    def setUp(self):
        self.task = self.task = Task.objects.create(initial_structure_type=1, number_of_beads=10)
        run_sm.delay(self.task.id)

    def tearDown(self):
        self.task.delete()

    def test_output_exist(self):
        """Test if pdb file with output is generated"""
        self.assertEqual(os.path.isfile(self.task.get_output_path()), True)

    def test_make_distance_map(self):
        """Test if png file with distance is generated"""
        self.assertEqual(os.path.isfile(self.task.get_distance_map_path()), True)
