from django.conf import settings
from django.test import TestCase

from ..forms import NewTaskForm


class TestNewTaskForm(TestCase):
    simple_data_set_1 = {
        'restraints': "20 90 <>",
        'initial_structure_type': "1",
        'stiffness': '10'
    }

    simple_data_set_2 = {
        'interactions': "chr7 10 20 chr7 50 60 7",
        'initial_structure_type': "1",
        'stiffness': '10',
        'region': 'chr7:1-100',
        'resolution': '5',
    }

    def test_form_creation(self):
        NewTaskForm()

    ###############################################
    #    Single fields validation - restraints    #
    ###############################################

    def test_clean_restraints_1(self):
        form = NewTaskForm(data=self.simple_data_set_1)
        form.is_valid()
        print(form.errors)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['restraints'], [(20, 90, 'H')])

    def test_clean_restraints_2(self):
        data_set = dict(self.simple_data_set_1)
        data_set['restraints'] = "20 90"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['restraints'], [(20, 90, "N")])

    def test_clean_restraints_3(self):
        data_set = dict(self.simple_data_set_1)
        data_set['restraints'] = "20 90\n30 80 <>\n40 90\n40 120 <>"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['restraints'],
                         [(20, 90, "N"), (30, 80, 'H'), (40, 90, "N"), (40, 120, 'H')])

    def test_clean_restraints_4(self):
        data_set = dict(self.simple_data_set_1)
        data_set['restraints'] = "dupa"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), False)
        self.assertEqual(form.errors['restraints'][0], 'Error in restraints in line 1')

    def test_clean_restraints_5(self):
        data_set = dict(self.simple_data_set_1)
        data_set['restraints'] = "20 90 >>"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['restraints'], [(20, 90, "C")])

    def test_clean_restraints_6(self):
        data_set = dict(self.simple_data_set_1)
        data_set['restraints'] = "20 90 C"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['restraints'], [(20, 90, "C")])

    def test_clean_restraints_7(self):
        data_set = dict(self.simple_data_set_1)
        data_set['restraints'] = "20 90 H"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['restraints'], [(20, 90, "H")])

    def test_clean_restraints_8(self):
        data_set = dict(self.simple_data_set_1)
        data_set['restraints'] = "20\t90\tH"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['restraints'], [(20, 90, "H")])

    def test_clean_restraints_9(self):
        data_set = dict(self.simple_data_set_1)
        data_set['restraints'] = "20\t20"
        form = NewTaskForm(data=data_set)
        form.is_valid()
        print(form.errors)
        self.assertEqual(form.is_valid(), False)
        self.assertFalse('restraints' in form.cleaned_data)

    def test_clean_restraints_10(self):
        data_set = dict(self.simple_data_set_1)
        data_set['restraints'] = "20\t21"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), False)
        self.assertFalse('restraints' in form.cleaned_data)

    def test_clean_restraints_11(self):
        data_set = dict(self.simple_data_set_1)
        data_set['restraints'] = "20\t22"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['restraints'], [(20, 22, "N")])

    #################################################
    #    Single fields validation - interactions    #
    #################################################

    def test_clean_interactions_1(self):
        form = NewTaskForm(data=self.simple_data_set_2)
        form.is_valid()
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['interactions'], [('7', 10, 20, '7', 50, 60, 7, 'N')])

    def test_clean_interactions_2(self):
        data_set = dict(self.simple_data_set_2)
        data_set['interactions'] = "chr7 10 20 chr7 50 60 7 >>"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['interactions'], [('7', 10, 20, '7', 50, 60, 7, 'C')])

    def test_clean_interactions_3(self):
        data_set = dict(self.simple_data_set_2)
        data_set['interactions'] = "chr7 10 20 chr7 50 60 7 <<"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['interactions'], [('7', 10, 20, '7', 50, 60, 7, 'C')])

    def test_clean_interactions_4(self):
        data_set = dict(self.simple_data_set_2)
        data_set['interactions'] = "chr7 10 20 chr7 50 60 7 <>"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['interactions'], [('7', 10, 20, '7', 50, 60, 7, 'H')])

    def test_clean_interactions_5(self):
        data_set = dict(self.simple_data_set_2)
        data_set['interactions'] = "chr7 10 20 chr7 50 60 7 ><"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['interactions'], [('7', 10, 20, '7', 50, 60, 7, 'H')])

    def test_clean_interactions_6(self):
        data_set = dict(self.simple_data_set_2)
        data_set['interactions'] = "chr7 10 20 chr7 50 60 7 C"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['interactions'], [('7', 10, 20, '7', 50, 60, 7, 'C')])

    def test_clean_interactions_7(self):
        data_set = dict(self.simple_data_set_2)
        data_set['interactions'] = "chr7 10 20 chr7 50 60 7 H"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)
        self.assertEqual(form.cleaned_data['interactions'], [('7', 10, 20, '7', 50, 60, 7, 'H')])

    def test_clean_number_of_beads_1(self):
        limit = settings.MAX_NUMBER_OF_BEADS
        data_set = dict(initial_structure_type="1", stiffness="10", number_of_beads=limit + 1)
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), False)

    def test_clean_number_of_beads_2(self):
        limit = settings.MAX_NUMBER_OF_BEADS
        data_set = dict(initial_structure_type="1", stiffness="10", number_of_beads=limit)
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), True)

    ############################################################
    #    Multiple fields validation - region + interactions    #
    ############################################################

    def test_clean_region_interactions(self):
        """All interactions must fit within range."""
        data_set = dict(self.simple_data_set_2)
        data_set['interactions'] = "chr7 10 20 chr7 90 101 7"
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), False)

    ###################################################################
    #    Multiple fields validation - number of beads + restraints    #
    ###################################################################

    def test_model_coords_must_not_exceed_number_of_beads(self):
        data_set = dict(self.simple_data_set_1)
        data_set['number_of_beads'] = 10
        form = NewTaskForm(data=data_set)
        self.assertEqual(form.is_valid(), False)
