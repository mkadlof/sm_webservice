import os

from django.test import TestCase, Client, override_settings
from django.urls import reverse


class TestNewTaskForm(TestCase):

    def tearDown(self):
        self.task.delete()

    @override_settings(CELERY_TASK_EAGER_PROPAGATES=True,
                       CELERY_TASK_ALWAYS_EAGER=True)
    def test_index(self):
        """In this test simple modelling should be run and finished after few second using web interface."""
        data = dict(initial_structure_type='1', number_of_beads='10', stiffness='0')
        client = Client()
        response = client.post(reverse('index'), data=data)
        self.task = response.context['task']
        self.task.refresh_from_db()
        self.assertEqual('F', self.task.status)
        self.assertTrue(os.path.isfile(self.task.get_initial_structure_path()))
        self.assertTrue(os.path.isfile(self.task.get_plot_arcs_path()))
        self.assertTrue(os.path.isfile(self.task.get_distance_map_path()))
