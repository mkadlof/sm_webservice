import re

from django import forms
from django.conf import settings
from django.forms import ModelForm, ValidationError

from .models import Task


class NewTaskForm(ModelForm):
    class Meta:
        model = Task
        exclude = ['timestamp', 'ip_address', 'status', 'final_structure_potential_energy', 'raw_input']
        widgets = {
            'initial_structure_type': forms.RadioSelect(),
        }

    restraints = forms.CharField(widget=forms.Textarea(attrs={'cols': 15, 'rows': 15}), required=False)
    region = forms.CharField(help_text="Genomic region in format chrN:xxx-yyy", required=False)
    interactions = forms.CharField(widget=forms.Textarea(attrs={'cols': 65, 'rows': 15}), required=False,
                                   help_text='BEDPE format.')

    coordinates_type = forms.CharField(required=False, widget=forms.HiddenInput)

    @staticmethod
    def _parse_motif(s):
        if s in ['>>', '<<', 'C']:
            return 'C'
        elif s in ['><', '<>', 'H']:
            return 'H'
        elif not s:
            return "N"
        else:
            raise ValidationError(f"I Can't parse directionality motif: {s}")

    def clean_restraints(self):
        """
        Restraints are interactions between beads of model expressed in model bead indices coordinates (integers).
        Additionally loop type can be provided as one of following strings: <<, <>, ><, >>. They correspond to CTCF
        protein motif directionality.

        Restraints are provided as a string tab or space delimited with every restraint in single line.
        They are validated using regexp.

        :return list: list of tuples (int, int, str)
        :raises ValidationError:
        """
        if self.cleaned_data.get('restraints') == '':
            return []
        data = self.cleaned_data.get('restraints').split('\n')
        data = [i.rstrip() for i in data]
        regexp = re.compile(r'^(\d+)[\t ]+(\d+)(?:[ \t]+([><]{2}|[HC])?)?\s*$')
        cleaned_data = []
        for i, line in enumerate(data):
            match = regexp.match(line)
            if not match:
                raise ValidationError(f"Error in restraints in line {i + 1}")
            else:
                rst_1, rst_2, motif = match.groups()
                rst_1 = int(rst_1)
                rst_2 = int(rst_2)
                if rst_1 > rst_2:
                    raise ValidationError(f'First coordinate must be smaller than second: {rst_1} {rst_2}')
                if rst_1 + 1 >= rst_2:
                    raise ValidationError(f'Restraints can not be defined between two neighbouring beads. They must be separated by at least one bead: {rst_1} {rst_2}')
                loop_type = self._parse_motif(motif)
                cleaned_data.append((rst_1, rst_2, loop_type))
        return cleaned_data

    def clean_interactions(self):
        """
        Interactions are contacts between segments of chromatin fiber determined by biological experiment. For example
        ChIA-PET, Hi-C, etc... It is convenient to provide interactions instead of restraints. BEDPE file format is
        mandatory. All columns after 7'th are ignored.

        :return list: list of tuples (chr1, begin1, end1, chr2, begin2, end2, value, ...)
        :raises ValidationError
        """
        # TODO This will need to be refactored when CTCF directionality will be added.
        if self.cleaned_data.get('interactions') == '':
            return []
        data = self.cleaned_data.get('interactions').split('\n')
        data = [i.rstrip() for i in data]
        regexp = re.compile(r'^[Cc]hr(..?)(?:\t| +)'  # chr1
                            r'([1-9][0-9]*)(?:\t| +)'  # begin1
                            r'([1-9][0-9]*)(?:\t| +)'  # end1
                            r'[Cc]hr(..?)(?:\t| +)'  # chr2
                            r'([1-9][0-9]*)(?:\t| +)'  # begin2
                            r'([1-9][0-9]*)(?:\t| +)'  # end2
                            r'([1-9][0-9]*)(?:[\t ]+(>>|><|<>|<<|C|H)?)?$')  # value and others

        cleaned_data = []
        for i, line in enumerate(data):
            match = regexp.match(line)
            if not match:
                raise ValidationError(f"Error in interactions in line {i + 1}")
            else:
                chr1, begin1, end1, chr2, begin2, end2, value, other = match.groups()
                begin1, end1, begin2, end2, value = [int(i) for i in [begin1, end1, begin2, end2, value]]
                if not (begin1 <= end1 <= begin2 <= end2):
                    raise ValidationError(f'Error in coordinates in line {i + 1}')
                loop_type = self._parse_motif(other)
                cleaned_data.append((f'{chr1}', begin1, end1, f'{chr2}', begin2, end2, value, loop_type))

        # Check if chromosomes ID comes from single Chromosome.
        chromosomes_1, _, _, chromosomes_2, *_ = list(zip(*cleaned_data))
        chromosomes = set(chromosomes_1) | set(chromosomes_2)
        if len(chromosomes) > 1:
            raise ValidationError(f'More than one different chromosome seen: {list(chromosomes)}')
        return cleaned_data

    def clean_region(self):
        region = self.cleaned_data.get('region')
        if region == '' or region is None:
            return ''
        regexp = re.compile('^[Cc]hr(..?):([1-9][0-9]*)-([1-9][0-9]*)$')
        match = regexp.match(region)
        if not match:
            raise ValidationError("Invalid region string")
        else:
            chrom, begin, end = match.groups()
        begin, end = int(begin), int(end)
        if end < begin:
            raise ValidationError("Region validation error: End begin greater than end.")
        return chrom, begin, end

    def clean_resolution(self):
        resolution = self.cleaned_data.get('resolution')
        if resolution == '' or resolution is None:
            return None
        else:
            resolution = int(resolution)
            if not (5 <= resolution <= 1e6):
                raise ValidationError("Resolution exceed available limits.")
        return resolution

    def clean_stiffness(self):
        stiffness = self.cleaned_data['stiffness']
        if not (0 <= stiffness <= 1e5):
            raise ValidationError("Stiffness must be between values 0 and 100000.")
        return self.cleaned_data['stiffness']

    def clean_number_of_beads(self):
        number_of_beads = self.cleaned_data['number_of_beads']
        if number_of_beads == '' or number_of_beads is None:
            return None
        if not (5 <= number_of_beads <= settings.MAX_NUMBER_OF_BEADS):
            raise ValidationError("Limit of number of beads exceeded.")
        return number_of_beads

    def clean(self):
        # Check the type of input data - model coords or genomic coords
        number_of_beads_provided = bool(self.cleaned_data.get('number_of_beads'))
        restraints_provided = bool(self.cleaned_data.get('restraints'))

        region_provided = bool(self.cleaned_data.get('region'))
        resolution_provided = bool(self.cleaned_data.get('resolution'))
        interactions_provided = bool(self.cleaned_data.get('interactions'))

        any_model_coords = restraints_provided or restraints_provided
        any_genomic_coords = region_provided or resolution_provided or interactions_provided

        # No data coordinates sections
        if not (number_of_beads_provided or restraints_provided or region_provided or resolution_provided
                or interactions_provided):
            raise ValidationError("No data provided.")

        # Some data in model section and some in genomic section
        if any_model_coords and any_genomic_coords:
            raise ValidationError("Ambiguous input.")

        # Data only in genomic section
        if not (number_of_beads_provided or restraints_provided):
            if not resolution_provided:
                raise ValidationError("I assume you're trying to use genomic coordinates but mandatory "
                                      "resolution field is empty.")
            self.cleaned_data['coordinates_type'] = 'genomic'

        # Data only in Model coordinates
        if not (region_provided or resolution_provided or interactions_provided):
            # every combination of fields in model coordinates section is valid
            self.cleaned_data['coordinates_type'] = 'model'

        # None of the interaction coords may lay outside of region
        # interactions must be sorted right way.
        if any_genomic_coords and region_provided:
            errors = []
            region = self.cleaned_data['region']
            chr_id, begin, end = region
            for k, i in enumerate(self.cleaned_data['interactions']):
                chr1, start1, end1, chr2, start2, end2, _, _ = i
                if not (chr_id == chr1 and chr_id == chr2):
                    errors.append(f"Error in interactions line {k + 1}: Chromosomes must be consistent with region")
                if not (begin <= start1 <= end1 <= start2 <= end2 <= end):
                    errors.append(
                        f"Error in interactions line {k + 1}: all coordinates must satisfy condition:"
                        f" region_begin ≤ start1 ≤ end1 ≤ start2 ≤ end2 ≤ region_end")
            if len(errors) > 0:
                raise ValidationError(errors)

        if restraints_provided and number_of_beads_provided:
            restraints = self.cleaned_data.get('restraints')
            number_of_beads = self.cleaned_data.get('number_of_beads')
            for restraint in restraints:
                a, b, _ = restraint
                if a > number_of_beads or b > number_of_beads:
                    raise ValidationError(f"Model coordinates must not exceed number of beads. Invalid coordinates:' {a} {b}. Number of beads: {number_of_beads}")
