def mid_point(a: int, b: int) -> int:
    """
    returns point in the middle between two points.
    :param a: int
    :param b: int
    :return: int
    """
    return (a + b) // 2


def translate_interactions_to_restraints(interactions: list, resolution: int, begin: int) -> list:
    """
    Transforms genomic coordinates into models beads coordinates.
    :param interactions: list of interactions (tuples)
    :param resolution: integer in bp units
    :param begin: position bp units)
    :return: list of coordinates and loop type (tuples) in model bead units
    """
    points = []
    for i in interactions:
        _, begin1, end1, _, begin2, end2, _, loop_type = i
        a = (mid_point(begin1, end1) - begin) // resolution + 1
        b = (mid_point(begin2, end2) - begin) // resolution + 1
        if a > b:
            a, b = b, a
        if a + 1 < b and (a, b, loop_type) not in points:
            points.append((a, b, loop_type))
    return points


def find_limits_in_interactions(interactions: list) -> tuple:
    """
    This function is used to determine region based on interactions if it is not provided explicit.
    :param interactions: List of interactions
    :return: tuple (chromosome_ID, begin, end)
    """
    chromosomes, begins1, ends1, _, begins2, ends2, *_ = list(zip(*interactions))
    chr_id = chromosomes[0]
    begin = min(begins1 + begins2)
    end = max(ends1 + ends2)
    return chr_id, begin, end


def get_or_none(model, *args, **kwargs):
    try:
        return model.objects.get(*args, **kwargs)
    except model.DoesNotExist:
        return None
