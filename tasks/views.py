import logging
import os

import numpy as np
from django.conf import settings
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.static import serve

from sm_engine.tasks import run_sm
from .forms import NewTaskForm
from .miscellanea_functions import translate_interactions_to_restraints, find_limits_in_interactions, get_or_none
from .models import Task, Restraint, BedpeRecord, Region

log = logging.getLogger('sm_logger')


def save_restraints(restraints: list, new_task: Task) -> None:
    """Save restraints to db and to file (for openMM script and for chimera)"""
    # TODO do we really need to save the files?
    w_openmm = ''
    w_chimera = ''
    records = []
    for i in restraints:
        rst_1, rst_2, loop_type = i
        w_openmm += f':{rst_1} :{rst_2} {settings.SPRING_LENGTH} {settings.SPRING_STRENGTH} {loop_type}\n'
        w_chimera += f':{rst_1} :{rst_2} red\n'
        records.append(
            Restraint(coord_1=rst_1, coord_2=rst_2, value=settings.SPRING_LENGTH, task=new_task, loop_type=loop_type))
    Restraint.objects.bulk_create(records)
    with open(new_task.get_restraints_openmm_path(), 'w') as f:
        f.write(w_openmm)
    with open(new_task.get_restraints_chimera_path(), 'w') as f:
        f.write(w_chimera)


def save_interactions(interactions: list, new_task: Task) -> None:
    # TODO do we need to save interactions to file like in restraints?
    records = []
    for i in interactions:
        chr1, begin1, end1, chr2, begin2, end2, value, loop_type = i
        records.append(
            BedpeRecord(chrom1=chr1, begin1=begin1, end1=end1, chrom2=chr2, begin2=begin2, end2=end2, value=value,
                        task=new_task, loop_type=loop_type))
    BedpeRecord.objects.bulk_create(records)


def calculate_number_of_beads(resolution: int, begin: int, end: int) -> int:
    return (end - begin) // resolution


@csrf_exempt  # TODO to be removed when normal API will be created
def index(request):
    if request.method == 'POST':
        form = NewTaskForm(request.POST, request.FILES)
        if 'resubmit' in form.data:
            context = {'form': form}
            return render(request, template_name='tasks/new_task_form.html', context=context)
        if form.is_valid():
            new_task: Task = form.save(commit=False)

            # Save form data in raw form to allow refiling the form latter and ease resubmitting it
            # with altered params.
            raw_input = dict(form.data)
            raw_input = {k: v[0] for k, v in raw_input.items()}
            new_task.raw_input = raw_input

            # Get IP address of user
            x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
            if x_forwarded_for:
                ip_address = x_forwarded_for.split(',')[-1].strip()
            else:
                ip_address = request.META.get('REMOTE_ADDR')
            new_task.ip_address = ip_address

            log.info(f'Task {new_task.id} submitted by {ip_address}')
            log.debug(f'{new_task}: {raw_input}')

            if form.cleaned_data['coordinates_type'] == "model":
                new_task.save()
                save_restraints(form.cleaned_data['restraints'], new_task)
            elif form.cleaned_data['coordinates_type'] == "genomic":
                region = form.cleaned_data['region']
                resolution = form.cleaned_data['resolution']
                interactions = form.cleaned_data['interactions']
                if not region:  # make region from coordinates
                    chrom, begin, end = find_limits_in_interactions(interactions)
                else:
                    chrom, begin, end = region
                region = Region(chromosome=chrom, begin=begin, end=end, task=new_task)
                new_task.number_of_beads = calculate_number_of_beads(resolution, region.begin, region.end)
                new_task.save()
                region.task = new_task
                region.save()
                restraints = translate_interactions_to_restraints(interactions, resolution, region.begin)
                save_interactions(interactions, new_task)
                save_restraints(restraints, new_task)

            # Here celery runs (celery task id is the same as spring-model id)
            run_sm.apply_async(args=[new_task.id], task_id=str(new_task.id))

            new_task.make_diagram_plot()
            context = {'task': new_task}
            return render(request, template_name='tasks/new_task_accepted.html', context=context)
    else:
        form = NewTaskForm()
    context = {'form': form}
    return render(request, template_name='tasks/new_task_form.html', context=context)


class TasksListView(ListView):
    model = Task
    ordering = ['-timestamp']
    paginate_by = 25


class TasksDetailView(DetailView):
    model = Task

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['restraints'] = Restraint.objects.filter(task=self.kwargs['pk'])
        context['interactions'] = BedpeRecord.objects.filter(task=self.kwargs['pk'])
        context['region'] = get_or_none(Region, task=self.kwargs['pk'])
        context['ticks'] = np.linspace(1, context['object'].number_of_beads, 11).astype(np.uint32)
        if context['region']:
            a = context['region'].begin
            b = context['region'].end
            context['ticks2'] = np.linspace(a, b, 6).astype(np.uint64)
            context['bppi'] = len(context['region']) / len(context['interactions'])
        context['effective_resolution'] = context['object'].effective_resolution
        context['form'] = NewTaskForm(context['object'].raw_input)
        return context


def serve_initial_structure(request, pk):
    task = Task.objects.get(pk=pk)
    return serve(request, os.path.basename(task.get_initial_structure_path()),
                 os.path.dirname(task.get_initial_structure_path()))


def serve_minimized_structure(request, pk):
    task = Task.objects.get(pk=pk)
    return serve(request, os.path.basename(task.get_output_path()), os.path.dirname(task.get_output_path()))


def serve_restraints(request, pk):
    task = Task.objects.get(pk=pk)
    return serve(request, os.path.basename(task.get_restraints_chimera_path()),
                 os.path.dirname(task.get_restraints_chimera_path()))


def serve_arcs_diagram(request, pk):
    task = Task.objects.get(pk=pk)
    return serve(request, os.path.basename(task.get_plot_arcs_path()),
                 os.path.dirname(task.get_plot_arcs_path()))


def serve_distance_map(request, pk):
    task = Task.objects.get(pk=pk)
    return serve(request, os.path.basename(task.get_distance_map_path()),
                 os.path.dirname(task.get_distance_map_path()))
