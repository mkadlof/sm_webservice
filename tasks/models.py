import logging
import os
import shutil
import uuid

from django.conf import settings
from django.db.models import JSONField
from django.db import models
from django_celery_results.models import TaskResult

from sm_utils.diagram_plot import make_arcs_diagram_plot
from sm_utils.generator import line, polymer_circle, baseball, self_avoiding_random_walk, center, random_walk
from sm_utils.points_io import save_points_as_pdb

log = logging.getLogger('sm_task')


class Task(models.Model):
    STATUS_CHOICES = [
        ('W', 'WAITING'),
        ('R', 'RUNNING'),
        ('F', 'FINISHED'),
        ('E', 'ERROR'),
    ]
    INITIAL_STRUCTURE_TYPE_CHOICES = [
        (0, 'line'),
        (1, 'circle'),
        (2, 'self-avoiding random walk'),
        (3, 'pure random walk'),
        (4, 'baseball')
    ]

    # Hidden fields
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    ip_address = models.GenericIPAddressField(default="0.0.0.0")
    status = models.CharField(
        max_length=1,
        choices=STATUS_CHOICES,
        default='W',
    )
    final_structure_potential_energy = models.FloatField(default=0)

    # Common data provided by user
    title = models.CharField(max_length=255, blank=True, help_text="Up to 255 characters")
    initial_structure_type = models.IntegerField(choices=INITIAL_STRUCTURE_TYPE_CHOICES, default=3)
    stiffness = models.FloatField(default=10.0, help_text="Values between 0 and 100 are suitable for most cases.")
    spherical_container = models.FloatField(default=None, blank=True, null=True,
                                            help_text="Diameter of container in σ (bead diameter) units. Leave empty "
                                                      "for modelling without this constraint.")
    refinement = models.BooleanField(default=False, help_text="May significantly extend computation time.")
    excluded_volume = models.BooleanField(default=True, help_text="Check to include ev term")

    # Specific use case fields
    number_of_beads = models.IntegerField(blank=True, null=True,
                                          help_text=f"The limits are: {settings.MIN_NUMBER_OF_BEADS} -"
                                          f" {settings.MAX_NUMBER_OF_BEADS}")
    resolution = models.IntegerField(blank=True, null=True,
                                     help_text="Values between 1000 - 100 000 are usually reasonable and"
                                               " strongly depend on size of modelling region")
    raw_input = JSONField(blank=True, null=True)

    def _create_task_directory(self):
        directory = os.path.join(settings.TASKS_FOLDER, str(self.id))
        os.mkdir(directory)
        log.debug(f'{self} directory created: {directory}')

    def _make_initial_structure(self):
        # Here is point centering. Currently it is (0,0,0). It may cause artifacts for very large structures, due to
        # PDB file format limitations.
        if self.initial_structure_type == 0:
            points = line(self.number_of_beads, displacement=0.1)
        elif self.initial_structure_type == 1:
            points = polymer_circle(self.number_of_beads, z_stretch=1 / self.number_of_beads)
        elif self.initial_structure_type == 2:
            points = self_avoiding_random_walk(self.number_of_beads, limit=settings.SAWR_MAXIMUM_TRIES)
        elif self.initial_structure_type == 3:
            points = random_walk(self.number_of_beads)
        elif self.initial_structure_type == 4:  # TODO make distances between beads equal. Efficiently!
            points = baseball(self.number_of_beads, self.number_of_beads ** 0.75)
        else:
            raise ValueError(f"initial_structure_type must be int 1, 2 or 3! It is {self.initial_structure_type}")
        points = center(points, (0., 0., 0.))
        initial_structure_path = self.get_initial_structure_path()
        save_points_as_pdb(points, initial_structure_path, verbose=False)
        log.debug(f'{self} initial structure created: {initial_structure_path}')

    def _make_empty_restraints_file(self):
        restraints_path = self.get_restraints_openmm_path()
        open(restraints_path, 'a').close()
        log.debug(f'{self}, Restraints file created: {restraints_path}')

    def make_diagram_plot(self):
        make_arcs_diagram_plot(self)
        log.debug(f'{self} arc diagram plotted.')

    def save(self, **kwargs):
        if not Task.objects.filter(id=self.id).exists():
            self._create_task_directory()
            self._make_initial_structure()
            self._make_empty_restraints_file()
        super().save(**kwargs)
        log.debug(f'{self} saved in DB.')

    def delete(self, **kwargs):
        """With task object delete also a corresponding task directory"""
        shutil.rmtree(self.get_path())
        super().delete(**kwargs)
        log.info(f'{str(self)} deleted.')

    def get_path(self):
        return os.path.join(settings.TASKS_FOLDER, str(self.id))

    def get_url(self):
        return f'/tasks/{self.id}'

    def get_initial_structure_url(self):
        return f'/tasks/{self.id}/initial_structure.pdb'

    def get_restraints_url(self):
        return f'/tasks/{self.id}/restraints.rst'

    def get_output_url(self):
        return f'/tasks/{self.id}/output.pdb'

    def get_arcs_url(self):
        return f'/tasks/{self.id}/arcs.png'

    def get_distance_map_url(self):
        return f'/tasks/{self.id}/distance_map.png'

    def get_initial_structure_path(self):
        return os.path.join(self.get_path(), 'initial_structure.pdb')

    def get_restraints_openmm_path(self):
        # TODO This should be refactored. OpenMM should get restraints z from database, not from HDD...
        return os.path.join(self.get_path(), 'restraints_openmm.rst')

    def get_restraints_chimera_path(self):
        return os.path.join(self.get_path(), 'restraints.rst')

    def get_output_path(self):
        return os.path.join(self.get_path(), 'output.pdb')

    def get_plot_arcs_path(self):
        return os.path.join(self.get_path(), 'arcs.png')

    def get_distance_map_path(self):
        return os.path.join(self.get_path(), 'distance_map.png')

    def get_number_of_restraints(self):
        return len(Restraint.objects.filter(task=self))

    def get_restraints(self):
        return Restraint.objects.filter(task=self.id)

    def set_status_running(self):
        previous_status = list(filter(lambda x: x[0] == self.status, self.STATUS_CHOICES))[0][1]
        self.status = 'R'
        self.save()
        current_status = list(filter(lambda x: x[0] == 'R', self.STATUS_CHOICES))[0][1]
        log.info(f'{self}: Status change: {previous_status} -> {current_status}')

    def set_status_finished(self):
        previous_status = list(filter(lambda x: x[0] == self.status, self.STATUS_CHOICES))[0][1]
        self.status = 'F'
        self.save()
        current_status = list(filter(lambda x: x[0] == 'F', self.STATUS_CHOICES))[0][1]
        log.info(f'{self}: Status change: {previous_status} -> {current_status}')

    def set_status_error(self):
        previous_status = list(filter(lambda x: x[0] == self.status, self.STATUS_CHOICES))[0][1]
        self.status = 'E'
        self.save()
        current_status = list(filter(lambda x: x[0] == 'E', self.STATUS_CHOICES))[0][1]
        log.info(f'{self}: Status change: {previous_status} -> {current_status}')

    @property
    def initial_structure_name(self):
        return list(filter(lambda x: x[0] == self.initial_structure_type, self.INITIAL_STRUCTURE_TYPE_CHOICES))[0][1]

    @property
    def beads_per_interaction(self) -> float:
        if self.get_number_of_restraints() != 0:
            return self.number_of_beads / self.get_number_of_restraints()
        else:
            return -1.0

    @property
    def effective_resolution(self):
        region = Region.objects.filter(task=self).first()
        if region:
            return len(region) / self.number_of_beads

    @property
    def short_id(self):
        return str(self.id)[:8]

    def __str__(self):
        return f'Task: {str(self.id)[:8]}'


class Restraint(models.Model):
    """
        Loop type allows three values: H - hairpin, C - coiled loop, N - no info about loop type
    """
    coord_1 = models.IntegerField()
    coord_2 = models.IntegerField()
    value = models.FloatField(default=0.2)
    loop_type = models.CharField(max_length=1, default='N')
    task = models.ForeignKey(Task, on_delete=models.CASCADE)

    def __iter__(self):
        yield self.coord_1
        yield self.coord_2

    def __str__(self):
        if self.loop_type != 'N':
            return f"{self.coord_1}\t{self.coord_2}\t{self.loop_type}"
        else:
            return f"{self.coord_1}\t{self.coord_2}"


class BedpeRecord(models.Model):
    """
        Loop type allows three values: H - hairpin, C - coiled loop, N - no info about loop type
    """
    chrom1 = models.CharField(max_length=5)
    begin1 = models.IntegerField()
    end1 = models.IntegerField()
    chrom2 = models.CharField(max_length=5)
    begin2 = models.IntegerField()
    end2 = models.IntegerField()
    value = models.IntegerField()
    loop_type = models.CharField(max_length=1, default='N')
    task = models.ForeignKey(Task, on_delete=models.CASCADE)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None, *args, **kwargs):
        if not (self.begin1 <= self.end1 <= self.begin2 <= self.end2):
            raise ValueError(f"Wrong coordinates in {str(self)}")
        super(BedpeRecord, self).save(*args, **kwargs)

    @property
    def begin(self):
        return (self.begin1 + self.end1) // 2

    @property
    def end(self):
        return (self.begin2 + self.end2) // 2

    def __str__(self):
        if self.loop_type != 'N':
            return f"chr{self.chrom1}\t{self.begin1}\t{self.end1}\tchr{self.chrom2}\t{self.begin2}\t{self.end2}\t" \
                f"{self.value}\t{self.loop_type}"
        else:
            return f"chr{self.chrom1}\t{self.begin1}\t{self.end1}\tchr{self.chrom2}\t{self.begin2}\t{self.end2}\t{self.value}"


class Region(models.Model):
    chromosome = models.CharField(max_length=2)
    begin = models.IntegerField()
    end = models.IntegerField()
    task = models.ForeignKey(Task, on_delete=models.CASCADE)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None, *args, **kwargs):
        self.chromosome = self.chromosome.lower()
        if self.end < self.begin:
            raise ValueError(f"End of region can not be after it's begin - begin {self.begin} end: {self.end}")
        super(Region, self).save(force_insert, force_update, *args, **kwargs)

    def __len__(self):
        return self.end - self.begin + 1

    def __str__(self):
        return f"chr{self.chromosome}:{self.begin}-{self.end}"
