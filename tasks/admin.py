from django.contrib import admin

from .models import Task


class TaskAdmin(admin.ModelAdmin):
    readonly_fields = ('id', 'ip_address', 'number_of_beads')
    fields = (('id', 'ip_address'), 'number_of_beads', 'title', 'status')


admin.site.register(Task, TaskAdmin)
