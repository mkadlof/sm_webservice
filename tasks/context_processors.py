from django.conf import settings


# noinspection PyUnusedLocal
def get_version(request):
    return {'VERSION': settings.VERSION}


# noinspection PyUnusedLocal
def google_analytics_key(request):
    return {'GOOGLE_ANALYTICS_KEY': settings.GOOGLE_ANALYTICS_KEY}
