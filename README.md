
SM-webservice works on:
 - python       3.7.1
 - django       2.1.5
 - openMM       7.3.0
 
Repo URL: [https://mkadlof@bitbucket.org/mkadlof/sm_webservice.git]

#### Environment
There is self deployment script: `setup_script.sh`.
It will install pyenv, set python and virtualenv, compile OpenMM.

#### Apply local settings
* `cd sm_webservice`
* `cp settings.py.dist settings.py`

#### Apply migrations
* `./reset_db.sh`

#### Run server
* `./manage.py runserver`