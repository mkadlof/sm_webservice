{% extends "base.html" %}
{% load static %}

{% block content %}
    <div class="sm-static-page" id="manual">
        <h1 class="countheads">Manual</h1>
        <p>This manual provides some key concepts important to understand how Spring Model works. For usage examples,
            with walkthrough instructions are available in <a href="{% url 'tutorial' %}">tutorial</a> section.</p>
        <h2 class="nocount">Topics</h2>
        <ol>
            <li><a href="#topic-1">Method overview</a></li>
            <li><a href="#topic-2">Fields description</a></li>
        </ol>
        <hr>

        <h2 class="countheads" id="topic-1">Method overview</h2>
        <p>Spring model is a webservice for modelling chromatin domains using direct information about contacts derived
            from experiments like <a href="https://science.sciencemag.org/content/326/5950/289">Hi-C</a>
            or <a href="https://www.nature.com/articles/nature08497">ChIA-PET</a>. It is employs polymer physics and
            molecular mechanics using <a href="http://openmm.org/">OpenMM</a> library. In SM a polymer is represented as
            a set of connected beads in 3D space. Their centers are distributed uniformly across the polymer chain i.e.
            distances between any two consecutive beads are constant and equal in bead diameter. User can choose type of
            initial structure, and define contacts using two formats of input (direct beads indices, and genomic loci).
            Then energy of system is minimized and structure folds.</p>

        <div class="md-row-img">
            <div class="image-with-caption">
                <img src="{% static 'img/ideogram_before.png' %}" alt="Initial structure">
                Initial structure
            </div>
            <div class="image-with-caption">
                <img src="{% static 'img/ideogram_after.png' %}" alt="Final structure">
                Final structure
            </div>
        </div>
        <hr>

        <h2 class="countheads" id="topic-2">Fields description</h2>
        <dl>
            <dt>Title</dt>
            <dd>Human readable description, helpful for task identification.</dd>

            <dt>Initial structure type</dt>
            <dd><p>Each chromatin model requires an initial structure. Spring model provides several options which may
                produce different results. The following options are available:</p>
                <ul>
                    <li>line
                        <p>Beads are distributed on an almost straight line. Every bead is slightly distorted in a
                            random
                            direction (maximal distortion length is 0.1<abbr title="bead radius">&sigma;</abbr>) to
                            break
                            symmetry.</p>
                    </li>
                    <li>circle
                        <p>Beads are distributed on a circle. The circle is slightly stretched in z-axis to break
                            symmetry.</p></li>
                    <li>self-avoiding random walk
                        <p>For models that are supposed to start from a random
                            conformation. This kind of random walk consists of spheres that have volume and they cannot
                            overlap.</p></li>
                    <li>pure random walk
                        <p>For certain kinds of modelling pure random walk may be a better choice. However, choosing
                            pure
                            random walk and Excluded Volume option may cause the energy of the system to explode due to
                            a high
                            rate of overlaps and cause your simulation to produce artifacts or end up with an error. We
                            recommend to choose this option only with disabled Excluded Volume option. Pure random walks
                            are usually more compacted than self-avoiding random walks.</p></li>
                    <li>baseball
                        <p>Beads are distributed on a sphere along the curve shaped like a baseball ball seam.</p>
                    </li>
                </ul>
                <p>Examples - Click image to download PDB file.</p>
                <div class="sm-row-img">

                    <div class="image-with-caption">
                        <a href="{% static 'examples/initial_structures_examples/pdb/line.pdb' %}"><img
                                src="{% static 'examples/initial_structures_examples/png/line.png' %}"
                                alt="image with example line initial structure"></a>Line<br>&nbsp;
                    </div>

                    <div class="image-with-caption">
                        <a href="{% static 'examples/initial_structures_examples/pdb/circle.pdb' %}"><img
                                src="{% static 'examples/initial_structures_examples/png/circle.png' %}"
                                alt="image with example circle initial structure"></a>Circle<br>&nbsp;
                    </div>

                    <div class="image-with-caption">
                        <a href="{% static 'examples/initial_structures_examples/pdb/sarw.pdb' %}"><img
                                src="{% static 'examples/initial_structures_examples/png/sarw.png' %}"
                                alt="image with example self-avoiding random walk initial structure"></a>Self-avoiding
                        random<br>&nbsp;
                        walk
                    </div>

                    <div class="image-with-caption">
                        <a href="{% static 'examples/initial_structures_examples/pdb/rw.pdb' %}"><img
                                src="{% static 'examples/initial_structures_examples/png/rw.png' %}"
                                alt="image with example pure random walk initial structure"></a>Pure random
                        walk<br>&nbsp;
                    </div>

                    <div class="image-with-caption">
                        <a href="{% static 'examples/initial_structures_examples/pdb/baseball.pdb' %}"><img
                                src="{% static 'examples/initial_structures_examples/png/baseball.png' %}"
                                alt="image with example baseball initial structure"></a>Baseball<br>&nbsp;
                    </div>
                </div>
            </dd>

            <dt>Stiffness</dt>
            <dd><p>This parameter controls the stiffness of a fiber. Technically it's a harmonic angle force defined
                between every three consecutive beads across the fiber. It's expressed in units of
                kJ/mol/rad<sup>2</sup>. 0 means that there is no stiffness at all. Any other positive value will add an
                energy term to make the polymer more rod-like than resembling boiled spaghetti.
                It will cause your loops to look more "inflated", and usually it makes the final structure visually
                cleaner.
                High values of this parameter may cause the stiffness to dominate the term associated with
                interaction energy, and will cause artifacts. For most cases a value of 20&thinsp;kJ/mol/rad<sup>2</sup>
                is a good first try, and usually it should not exceed 100&thinsp;kJ/mol/rad<sup>2</sup>. Remember
                that excluded volume also implies some extra stiffness to fiber.</p>

                <p>Examples - Click image to download PDB file.</p>
                <div class="sm-row-img">

                    <div class="image-with-caption">
                        <a href="{% static 'examples/stiffness_examples/pdb/stiffness10.pdb' %}"><img
                                src="{% static 'examples/stiffness_examples/png/stiffness10.png' %}"
                                alt="image with example structure with stiffness 10 kJ/mol/rad^2"></a>Stiffness<br>10&nbsp;kJ/mol/rad<sup>2</sup>
                    </div>
                    <div class="image-with-caption">
                        <a href="{% static 'examples/stiffness_examples/pdb/stiffness0.pdb' %}"><img
                                src="{% static 'examples/stiffness_examples/png/stiffness0.png' %}"
                                alt="image with example structure with stiffness 10 kJ/mol/rad^2"></a>Stiffness<br>0&nbsp;kJ/mol/rad<sup>2</sup>
                    </div>
                </div>
            </dd>

            <dt>Spherical container</dt>
            <dd><p>Modelling can be performed in an enclosed sphere-shaped volume. This parameter is the radius of
                this sphere expressed in <abbr title="bead radius">&sigma;</abbr>. Leave it empty if don't want to put
                any
                constraints on available volume.</p>

                <p>Examples - Click image to download PDB file.</p>
                <div class="sm-row-img">

                    <div class="image-with-caption">
                        <a href="{% static 'examples/spherical_container_examples/pdb/no_container.pdb' %}"><img
                                src="{% static 'examples/spherical_container_examples/png/no_container.png' %}"
                                alt="image with example structure built without spherical container"></a>No
                        container<br>&nbsp;
                    </div>
                    <div class="image-with-caption">
                        <a href="{% static 'examples/spherical_container_examples/pdb/container20.pdb' %}"><img
                                src="{% static 'examples/spherical_container_examples/png/container20.png' %}"
                                alt="image with example structure built with spherical container"></a>Container with
                        radius 20<abbr title="bead radius">&sigma;</abbr>
                    </div>
                </div>
            </dd>

            <dt>Refinement</dt>
            <dd><p>In some cases the energy minimization algorithm can get stuck in a local energy minimum or tend to
                produce
                artifacts. In such cases it can be helpful tu run several cycles of short molecular dynamics and energy
                minimization. The final structure will probably result with lower energy. However, depending on the
                modeling system it can take much more time. The refinement is performed with following steps:</p>
                <ol>
                    <li>Setting up the initial velocities using Boltzmann distribution and temperature
                        {{ temperature }}.
                    </li>
                    <li>Complete the following {{ n_cycles }} times:
                        <ol>
                            <li>Perform {{ n_steps }} steps of simulation using Verlet algorithm.</li>
                            <li>Perform energy minimization with rough convergence limit of {{ rough_limit }}</li>
                        </ol>
                    </li>
                    <li>Perform final energy minimization with fine convergence limit of {{ fine_limit }}.</li>
                </ol>
                <p>Without refinement there will be only one act of energy minimization with
                    coarse {{ rough_limit }} convergence condition.</p>
            </dd>

            <dt>Excluded volume</dt>
            <dd>Volume of the beads. By default it is included, excluding it will cause the beads to become points
                without volume, allowing the fiber to cross itself freely.
            </dd>

            <dt>Number of beads</dt>
            <dd>Optional. In case of restraints defined in model coordinates this parameter can control the length of
                the
                fiber. If it is left empty then the fiber will have the length of the maximum restraint coordinate + 1
                bead.
            </dd>

            <dt>Restraints</dt>
            <dd><p>Definition of long-range physical interactions - list of indices of connected beads. Each interaction
                has to be in a separate line. Coordinates have to be delimited with tab or space. Additionally one may
                add information about loop type using CTCF directionality notation
                (i.e. &gt;&gt; &gt;&lt; &lt;&gt; &lt;&lt;) or capital letters S for Stem loop and H for Hairpin loop.
                Vide example 3 on main site.</p>
                <p>Examples - Click image to download PDB file.</p>
                <div class="sm-row-img">

                    <div class="image-with-caption">
                        <a href="{% static 'examples/loop_types_examples/pdb/loop_types.pdb' %}"><img
                                src="{% static 'examples/loop_types_examples/png/loop_types.png' %}"
                                alt="image with example of loop types"></a>Coiled loop and hairpin loop
                    </div>
                </div>
            </dd>

            <dt>Region</dt>
            <dd>Optional. In case of modeling based on genomic coordinates this field can control the length of the
                fiber. If it is left empty then the length of the fiber will be deduced from the maximum coordinate in
                interactions field and the resolution.
            </dd>

            <dt>Resolution</dt>
            <dd>How much chromatin (number of base pairs) are represented by a single bead. The minimum value is 5 and
                maximum is limited by the total number of beads which should not exceed {{ max_number_of_beads }}.
            </dd>

            <dt>Interactions</dt>
            <dd>Definition of long-range physical interactions. List of interactions in BEDPE file. 7 columns are
                mandatory and 8th is optional. The middle point of an anchor is taken into account when translating
                genomic coordinates into bead indices. Value of 7th column is ignored. 8th column can be used to
                define the loop type in the same way as in the case of restraints.
            </dd>
        </dl>
    </div>
{% endblock %}