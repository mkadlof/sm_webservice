from django.conf import settings
from django.shortcuts import render


def overview(request):
    return render(request, 'static_pages/overview.html')


def tutorial(request):
    return render(request, 'static_pages/tutorial.html')


def manual(request):
    context = {
        'temperature': settings.DYN_TEMPERATURE,
        'n_steps': settings.NUMBER_OF_DYN_STEPS,
        'n_cycles': settings.NUMBER_OF_MIN_DYN_CYCLES,
        'rough_limit': settings.ROUGH_MIN_TOLERANCE,
        'fine_limit': settings.FINE_MIN_TOLERANCE,
        'max_number_of_beads': settings.MAX_NUMBER_OF_BEADS,
    }
    return render(request, 'static_pages/manual.html', context=context)


def contact(request):
    return render(request, 'static_pages/contact.html')


def privacy_policy(request):
    return render(request, 'static_pages/privacy_policy.html')
