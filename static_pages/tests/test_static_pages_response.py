from django.test import TestCase


class StaticPagesResponse(TestCase):

    def test_overview_response(self):
        url = '/overview'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_tutorial_response(self):
        url = '/tutorial'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_manual_response(self):
        url = '/manual'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_contact_response(self):
        url = '/contact'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_privacy_policy_response(self):
        url = '/privacy_policy'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
