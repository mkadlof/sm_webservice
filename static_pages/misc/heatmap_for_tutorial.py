import os
from math import sqrt, pi, exp
from typing import Tuple

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import LinearSegmentedColormap


def kth_diag_indices(a: np.ndarray, k: int):
    rows, cols = np.diag_indices_from(a)
    if k < 0:
        return rows[-k:], cols[:k]
    elif k > 0:
        return rows[:-k], cols[k:]
    else:
        return rows, cols


def distances_to_frequencies(distancemap: np.ndarray, a: float, k: float):
    return (distancemap / a) ** (-1 / k)


def add_noise(arr: np.ndarray, level: float):
    return arr + np.random.uniform(0, level, arr.shape)


def gauss(x, mi=0, sigma=1.):
    c = 1. / sqrt(2 * sigma ** 2 * pi)
    e = -((x - mi) ** 2) / (2 * sigma ** 2)
    return c * exp(e)


def add_gauss(arr: np.ndarray, point: Tuple[int, int], sigma: float = 1., factor: float = 1.):
    # stupid but works
    n = arr.shape[0]
    for i in range(n):
        for j in range(n):
            arr[i, j] += gauss(d((i, j), point), sigma=sigma) / factor
            arr[j, i] += gauss(d((i, j), point), sigma=sigma) / factor
    return arr


def d(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    return sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)


n = 30
dm = np.zeros((30, 30))
for i in range(30):
    dm[kth_diag_indices(dm, i)] = i
    dm[kth_diag_indices(dm, -i)] = i

hm = distances_to_frequencies(dm, a=1, k=5)

big_tad = tuple([slice(4, 28)] * 2)
tad_1 = tuple([slice(4, 19)] * 2)
tad_2 = tuple([slice(19, 28)] * 2)

hm[big_tad] = distances_to_frequencies(dm[big_tad], a=1, k=6)
hm[tad_1] = distances_to_frequencies(dm[tad_1], a=1, k=7)
hm[tad_2] = distances_to_frequencies(dm[tad_2], a=1, k=7)

hm = add_noise(hm, .02)
hm[big_tad] = add_noise(hm[big_tad], 0.03)
hm[tad_1] = add_noise(hm[tad_1], 0.04)
hm[tad_2] = add_noise(hm[tad_2], 0.04)

point1 = (4, 27)
point2 = (4, 18)
point3 = (19, 27)
hm = add_gauss(hm, point1, factor=2.5)
hm = add_gauss(hm, point2, factor=2.)
hm = add_gauss(hm, point3, factor=2.)

hm[np.diag_indices_from(hm)] = 1
hm = hm / hm.max()
hm[np.diag_indices_from(hm)] = 1

colors = [(1, 1, 1), (1, 0, 0), ]
cm = LinearSegmentedColormap.from_list('my_map', colors)

plt.imshow(hm, cmap=cm)

pos = [0] + list(range(4, 30, 5))
lab = [1] + list(range(5, 35, 5))
plt.xticks(pos, lab, fontsize=14)
plt.yticks(pos, lab, fontsize=14)
ax = plt.gca()

# webserver version

fname = 'static_dev/img/artificial_heatmap.png'
if not os.path.isfile(fname):
    plt.savefig('static_dev/img/artificial_heatmap.png', bbox_inches="tight", pad_inches=0.1)
else:
    raise ValueError('File already exist!')

plt.show()
