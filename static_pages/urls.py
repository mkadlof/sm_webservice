from django.urls import path

from . import views

urlpatterns = [
    path('overview', views.overview, name='overview'),
    path('tutorial', views.tutorial, name='tutorial'),
    path('manual', views.manual, name='manual'),
    path('contact', views.contact, name='contact'),
    path('privacy_policy', views.privacy_policy, name='privacy_policy'),
]
