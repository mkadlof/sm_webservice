import logging
import traceback
from math import pi

# noinspection PyPackageRequirements
import openmm as mm
from celery import shared_task
from django.conf import settings
# noinspection PyPackageRequirements
from openmm import *
# noinspection PyPackageRequirements
from openmm.app import *
# noinspection PyUnresolvedReferences,PyPackageRequirements
from openmm.unit import femtoseconds, kilojoule_per_mole

from sm_utils.distance_map import make_distance_map
from tasks.models import Task

log = logging.getLogger('sm_task')


@shared_task
def run_sm(task_id: str) -> None:
    task = Task.objects.get(pk=task_id)
    log.info(f'{task} started.')
    task.set_status_running()
    # noinspection PyPep8,PyBroadException
    try:
        sm_forcefield = ForceField(settings.FORCEFIELD_PATH)
        flat_bottom_force = mm.CustomBondForce('step(r-r0) * (k/2) * (r-r0)^2')
        flat_bottom_force.addPerBondParameter('r0')
        flat_bottom_force.addPerBondParameter('k')
        dire_force = mm.PeriodicTorsionForce()

        pdb = PDBFile(task.get_initial_structure_path())
        system = sm_forcefield.createSystem(pdb.topology, nonbondedMethod=CutoffNonPeriodic)
        n = system.getNumParticles()

        # Restraints force
        system.addForce(flat_bottom_force)
        system.addForce(dire_force)
        with open(task.get_restraints_openmm_path()) as input_file:
            for line in input_file:
                columns = line.split()
                atom_index_i = int(columns[0][1:]) - 1
                atom_index_j = int(columns[1][1:]) - 1
                r0 = float(columns[2])
                k = float(columns[3])
                loop_type = columns[4]
                flat_bottom_force.addBond(atom_index_i, atom_index_j, [r0, k])
                if loop_type == "C":
                    dire_force.addTorsion(atom_index_i + 1, atom_index_i, atom_index_j, atom_index_j - 1,
                                          periodicity=1, phase=0.0, k=settings.DIRE_FORCE)
                    dire_force.addTorsion(atom_index_i - 1, atom_index_i, atom_index_j, atom_index_j + 1,
                                          periodicity=1, phase=0.0, k=settings.DIRE_FORCE)
                elif loop_type == "H":
                    dire_force.addTorsion(atom_index_i + 1, atom_index_i, atom_index_j, atom_index_j - 1,
                                          periodicity=1, phase=pi, k=settings.DIRE_FORCE)
                    dire_force.addTorsion(atom_index_i - 1, atom_index_i, atom_index_j, atom_index_j + 1,
                                          periodicity=1, phase=pi, k=settings.DIRE_FORCE)

        # Spherical container force
        if task.spherical_container:
            # Here we assume that that structure is centered at (0, 0, 0)
            # User provides diameter in bead diameter unit (1 angstrom).
            # They must be converted into nanometers (default OpenMM length unit) and doubled since d = 2r
            container_force = mm.CustomExternalForce(
                '{}*max(0, r-{})^2; r=sqrt((x-{})^2+(y-{})^2+(z-{})^2)'.format(settings.SPHERICAL_CONTAINER_SCALE,
                                                                               task.spherical_container / 20, 0., 0.,
                                                                               0.))
            system.addForce(container_force)
            for i in range(n):
                container_force.addParticle(i, [])

        # Stiffness force
        stiffness = HarmonicAngleForce()
        system.addForce(stiffness)
        for i in range(n - 2):
            # angle in radians and k in kJ/mol/radian^2
            stiffness.addAngle(i, i + 1, i + 2, angle=pi, k=task.stiffness)

        # Excluded volume
        log.debug(f'Task: {task.short_id}, EV: {task.excluded_volume}')
        if task.excluded_volume:
            ev_force = mm.CustomNonbondedForce('epsilon*((2*sigma)/r)^12')
            ev_force.setCutoffDistance(2 * settings.EV_SIGMA)  # This line seems not working. Don't know why.
            ev_force.addGlobalParameter('epsilon', settings.EV_EPSILON)
            ev_force.addGlobalParameter('sigma', settings.EV_SIGMA)
            system.addForce(ev_force)
            for i in range(n):
                ev_force.addParticle()
            ev_force.createExclusionsFromBonds([(x, x + 1) for x in range(n - 1)], 1)
            log.debug(f'Task: {task.short_id}, EV_exclusions: {ev_force.getNumExclusions()}')

        integrator = VerletIntegrator(settings.DYN_TIME_STEP)

        # noinspection PyTypeChecker,PyCallByClass
        sm_platform = mm.Platform.getPlatformByName('CPU')
        sm_platform.setPropertyDefaultValue('Threads', str(settings.N_CPUS))

        sm_simulation = Simulation(pdb.topology, system, integrator, sm_platform)
        sm_simulation.context.setPositions(pdb.positions)
        log.debug(f'Task: {task.short_id}, Simulation...')
        sm_simulation.minimizeEnergy(tolerance=settings.ROUGH_MIN_TOLERANCE)
        if task.refinement:
            sm_simulation.context.setVelocitiesToTemperature(settings.DYN_TEMPERATURE)
            for i in range(settings.NUMBER_OF_MIN_DYN_CYCLES):
                log.debug(f'{task}, Refinement cycle {i + 1}/{settings.NUMBER_OF_MIN_DYN_CYCLES}...')
                sm_simulation.step(settings.NUMBER_OF_DYN_STEPS)
                sm_simulation.minimizeEnergy(tolerance=settings.ROUGH_MIN_TOLERANCE)
            sm_simulation.minimizeEnergy(tolerance=settings.FINE_MIN_TOLERANCE)
        final_structure_potential_energy = sm_simulation.context.getState(getEnergy=True).getPotentialEnergy()
        task.final_structure_potential_energy = final_structure_potential_energy.value_in_unit(kilojoule_per_mole)
        log.debug(f'{task} final structure energy {final_structure_potential_energy}')
        if final_structure_potential_energy >= settings.SUSPICIOUS_ENERGY:
            log.warning(f'{task} ended with suspiciously high energy {final_structure_potential_energy}.')
        sm_simulation.step(1)
        state = sm_simulation.context.getState(getPositions=True)
        with open(task.get_output_path(), 'w') as f:
            PDBFile.writeFile(pdb.topology, state.getPositions(), f)
        make_distance_map(task)
        task.set_status_finished()
    except Exception:
        log.error(f'{task} Oh shit! Something gone wrong! :(')
        task.set_status_error()
        log.error(traceback.format_exc())
